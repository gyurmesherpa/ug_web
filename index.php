
<?php
	session_start();
	define("DEFAULT_TIMEZONE", "Asia/Kathmandu");
	function mainRequestValue()
  	{
   	 if(isset($_POST['request'])){
   	   return $_POST['request'];
   	 }
   	 elseif (isset($_GET['request'])) {
   	   return $_GET['request'];
   	 }
   	 else {
   	   return "unset";
    	}
 	 }

	$req = mainRequestValue();

	function setUserAuthDetails(){
	   if(isset($_COOKIE['auth_user_id'])){
		define("USER_ID_AUTH", $_COOKIE['auth_user_id']);
   	 	//$userIdAuth = $_POST['auth_user_id'];
   	   }
   	   else{
		define("USER_ID_AUTH", "");
   	   	//$userIdAuth = "TestUserID";
   	   }
   	   if(isset($_COOKIE['auth_user_key'])){
		define("AUTH_KEY",$_COOKIE['auth_user_key']);
   	 	//$authKey = $_POST['auth_user_key'];
   	   }
   	   else{
		define("AUTH_KEY","");
   	   	//$authKey = "TestUserID";
   	   }
	}

  	setUserAuthDetails();

	if($req=='unset'){
		if(isset($_GET['u'])){
			if ($_GET['u'] != "") {
				unset($_SESSION[$_GET['u']]);
				setcookie('auth_user_id',"",time()+(60*30));
				 setcookie('auth_user_key',"",time()+(60*30));
			}
			else {
					unset($_SESSION[$_COOKIE['auth_user_id']]);
					setcookie('auth_user_id',"",time()+(60*30));
					 setcookie('auth_user_key',"",time()+(60*30));
			}
		}
		include('./front/index.php');
	}
	elseif($req=='register_page'){
		include('./front/register.php');

	}
	elseif($req=='home_page'){
		include('./front/index.php');

	}
	elseif($req=='faq_page'){
		include('./front/faq.php');
	}
	elseif($req == 'savedAdsPage'){
		include('./front/savedAds.php');
	}
	elseif($req=='add_issue'){
		require_once('./back/logics/sellerTasks.php');
		$st = new SellerTasks();
		$st->getAllCategories();
		define("ITEM_CATEGORIES", $st->getAllCategories());
		include('./front/sell.php');
	}
	// elseif($req=='set_authentication'){
	// 	echo AUTH_KEY;
	// }
	elseif($req == "advertisements"){
		define("ADVERTISEMENT_OPTION", $_POST['option']);
		define("DATE_DIRECTION", $_POST['date_d']);
		define("PRICE_DIRECTION", $_POST['price_d']);

		include './back/advertisements.php';
	}
	elseif($req == 'register_new_user'){
		include './back/logics/userTasks.php';
		include './back/models/user.php';
		include './back/logics/hashing.php';
		include './back/logics/dateTime.php';
		$hasher = new Hashing(trim($_POST['user_password']));
		$newUser = new User($_POST['user_first_name'], $_POST['user_last_name'], $_POST['user_email'], $hasher->getPwdHash(), $_POST['user_address'], $_POST['user_contact_number']);
		$cDT = new CustomDateTime();
		$newUser->setUserId("uid-".$cDT->getDateTimeStamp());
		$ut = new UserTasks();
		if($ut->registerNewUser($newUser)){
			echo "Register Success";
		}
		else{
			echo "Register Fail";
		}


	}
	elseif ($req == 'user_login') {
		include './back/logics/authentication.php';
		include './back/logics/userTasks.php';
		$userTasks = new UserTasks();
		$userDetails = $userTasks->getUserDetails($_POST['user_email']);
		//echo $userDetails->getUserId();
		$auth = new Authenticator($userDetails,$_SERVER['REQUEST_TIME']);
		if ($auth->pwdMatchCheck(trim($_POST['user_password'])) == "match")
		{
			$user_id = $userDetails->getUserId();
			$auth_key = $auth->generateAuthKey();
			setcookie('auth_user_id',$user_id,time()+(60*30));
			 setcookie('auth_user_key',$auth_key,time()+(60*30));
		    echo "{\"user_id\" : \"$user_id\", \"auth_key\" : \"$auth_key\"}";
		    //echo "loginSucess";

		}
		else
		{
			echo "error";
		}
	}
	elseif ($req == "adv_submission") {
		$animal = $_POST['product_type'];
		$issue = $_POST['ad_title'];
		$symptoms = $_POST['product_description'];
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "INSERT INTO animal_related_issues(animal,issue,symptoms_text) VALUES ('$animal','$issue', '$symptoms')";
			//$stmt->bind_param("sssssss", $user_id,$firstname, $lastname, $email, $password, $contactNo, $addressOne);
		$success = $dbConn->query($sql);
		$dbConn->close();
		if($success){
			//echo "Sales Added Sucessfully";
			echo "Successfully added";
		}
	}
	elseif ($req =="ad-description-page"){
		define("AD_ID", $_GET['aid']);
		include './front/ad-description-page.php';

	}
	elseif ($req =="confirm_issue"){
		define("ISSUE_ID", $_POST['issue_report_id']);
		define("CONFIRMED_ISSUE", $_POST['confirmed_issue']);
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
			$sql = "update reported_animal_related_issues set expert_issue_confirmation = '".CONFIRMED_ISSUE."' where issue_report_id = '".ISSUE_ID."'";
			$success = $dbConn->query($sql);

				if($sucess >= 1){
					echo "successfully updated";
				}
				else {
					echo "failed";
				}
				$dbConn->close();

	}
	elseif ($req == "commentSubmission") {
		include './back/logics/buyerTasks.php';
		include './back/models/onSaleItem.php';
		include './back/logics/authentication.php';
		include './back/models/user.php';
		include './back/logics/dateTime.php';
		$dt = new CustomDateTime();
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		$bt = new BuyerTasks();

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){

			if($bt->postComment($dt->getDateTimeStamp(),$dt->getMySqlTimeStamp(), $_POST['comment'], $_POST['adId'], 'comment', $_POST['commenterId'])){
				echo "comment_posted";
			}
			else{
				echo "comment_error";
			}
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}
	elseif ($req == 'replyComment') {
		include './back/logics/sellerTasks.php';
		include './back/models/onSaleItem.php';
		include './back/logics/authentication.php';
		include './back/models/user.php';
		include './back/logics/dateTime.php';
		$dt = new CustomDateTime();
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		$bt = new SellerTasks();

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){

			if($bt->replyComment($dt->getDateTimeStamp(),$dt->getMySqlTimeStamp(), $_POST['comment'], $_POST['adId'], 'reply', $_POST['commenterId'], $_POST['commentId'] )){
				echo "comment_posted";
			}
			else{
				echo "comment_error";
			}
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}
	elseif ($req == 'placeOrder') {
		include './back/logics/buyerTasks.php';
		include './back/logics/authentication.php';
		include './back/models/user.php';
		include './back/logics/dateTime.php';
		$dt = new CustomDateTime();
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		$bt = new BuyerTasks();
		$orderId = $dt->getDateTimeStamp();
		$orderDate = $dt->getMySqlTimeStamp();
		$status = "order_placed";
		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			echo $bt->placeOrder($orderId, $orderDate, addslashes($_POST['orderMsg']), $_POST['auth_user_id'], $_POST['adId'], $status);

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}

	elseif ($req == 'sellerPage') {
		require_once('./front/userProfile.php');

	}

	elseif ($req == 'showComments') {
		define("AD_ID", $_REQUEST['adId']);
		require_once('./back/comments.php');
	}

	elseif ($req == 'user_messagePage') {
		require_once('./front/user-message.php');
	}
	elseif($req == 'showBuyerMsg'){
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			define("MSG_TYPE", $_POST['typeMsg']);
			require_once('./back/messageCount.php');

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}

	}
	elseif ($req == 'showSellerMsg') {
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			define("MSG_TYPE", $_POST['typeMsg']);
			require_once('./back/messageCount.php');

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}
	elseif ($req == 'showProductMsg') {
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			define("SALES_ID", $_POST['salesId']);
			require_once('./back/messageText.php');

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}

		elseif ($req == 'acceptOrder') {
		include './back/logics/sellerTasks.php';
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$st = new SellerTasks();
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			if ($st->acceptOrder($_POST['adId'], $_POST['orderId'], $_POST['orderMsg'])) {
				echo "Order_Accp";
			}
			else{
				echo "error";
			}

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}

	elseif($req == 'showSellerMsg'){
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}
	elseif($req == 'showAcceptedSellerMsg'){
		include './back/logics/authentication.php';
		include './back/models/user.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);

		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			define("SALES_ID", $_POST['salesId']);
			require_once('./back/messageSellerText.php');

		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";

		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}
	elseif ($req == 'rateSeller') {
		include './back/logics/authentication.php';
		include './back/models/user.php';
		include './back/logics/buyerTasks.php';

		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		$bt = new BuyerTasks();
		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			if($bt->rateSeller ($_POST['adId'], $_POST['rating'], $_POST['feedback'])){
				echo "done";
			}
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";
		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}

	elseif($req == 'saveAd' || $req == 'removeSavedAd'){
		include './back/logics/authentication.php';
		include './back/models/user.php';
		include './back/logics/buyerTasks.php';
		$userDetails = new User("", "", "", "", "", "");
		$userDetails->setUserId(USER_ID_AUTH);
		$au = new Authenticator($userDetails, $_SERVER['REQUEST_TIME']);
		$bt = new BuyerTasks();
		if($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "All Approved"){
			if ($req == 'saveAd') {
				if($bt->saveAd($_POST['adId'], $_POST['auth_user_id'])){
					echo "done";
				}
			}
			elseif($req == 'removeSavedAd'){
				if($bt->removeSavedAd($_POST['adId'], $_POST['auth_user_id'])){
					echo "done";
				}
			}
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Session Expired") {
			//echo "Session Expired";
		    echo "Fail 2";
		}
		elseif ($au->authenticate(AUTH_KEY, $_SERVER['REQUEST_TIME']) == "Wrong Auth Key Sent") {
			//echo "Wrong Auth Key Sent";
			//Security Breached. Email to developer must be added.
			echo "Fail 3";
		}
		else{
			//echo "Session Not Set.\nLogin Again";
			echo "Fail 4";
		}
	}


	else
	{
			include('./front/register.php');
	}
?>
