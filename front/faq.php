<?php
include('header.php');
?>
<!DOCTYPE html>
	<html>
		<head>
			<title>Index</title>
		</head>	
			<div class="faq-lists">
				<button type="button" onclick="buy();">How to Buy?</button>
				<button type="button" onclick="sell();">How to Sell?</button>
				<button type="button" onclick="safety();">Safety Tipss</button>
			</div>
			<div class="faq-display">
				<div class="how-to-buy" id="how-to-buy-text">
					<p>
						
						<b>Step 1:</b> Browse through appropriate category as our site has been divided into categories such as Automobiles, Electronics, etc.<br>

						<b>Step 2:</b> Click on the product you wish to see the details.<br>

						<b>Step 3:</b> There are no sign-up forms to fill out unlike other services and you do not have to be a member to purchase any products as sellers information are directly accessible by clicking at product you want to purchase.<br>

						<b>Note:</b> Please arrange meetings with the seller at public places only. For more read safety tips.
					</p>
					
				</div>

	   			<div class="how-to-sell" id="how-to-sell-text">
					<p>
						
						<b>Step 1:</b> You can start selling online immediately once you register your account online. You can post your classifieds, edit existing classified, delete your classifieds and more as soon as you register.<br>

						<b>Step 2:</b>  You need to select an approriate category to post your classified. Then you need to fill out the neccessary information for that product. If you have a photo of the product you can also upload it.<br>

						<b>Step 3:</b>  This site manages every aspect of the e-commerce process and all you do is sit back, relax and just wait for your buyer's to call or email you.<br>

					</p>
					
				</div>

				<div class="safety-tips" id="safety-tips-text">
					<p>
						
						<b>Step 1:</b> Browse through appropriate category as our site has been divided into categories such as Automobiles, Electronics, etc.<br>

						<b>Step 2:</b> Click on the product you wish to see the details.<br>

						<b>Step 3:</b> There are no sign-up forms to fill out unlike other services and you do not have to be a member to purchase any products as sellers information are directly accessible by clicking at product you want to purchase.<br>


					</p>
					
				</div>

			</div>
		

		<script type="text/javascript" src= "./front/js/link.js"></script>

</body>

		
</html>