
<!DOCTYPE html>
	<html>
		<head>
			<title>Index</title>
		</head>	
		<body>
			<?php
				include('header.php');
			?>
			<?php
				require_once('./back/db/dbConnect.php');
				include './back/models/onSaleItem.php';

				$dbConn = connectDb();
				$uId = USER_ID_AUTH;
				$sql = "SELECT ad_id FROM saved_ads WHERE uid = '$uId'";
				$result = $dbConn->query($sql);
				$products=array();

				while($row = $result->fetch_assoc()) {
					$itemId = $row['ad_id'];
					$osi = new OnSaleItem();
					$sql2 = "SELECT item_id, price, item_images_json FROM on_sale_items WHERE item_id = '$itemId' AND sold_status <>'order_accepted'";
					$result2 = $dbConn->query($sql2);
					while($row2 = $result2->fetch_assoc()){
						$osi->getDetailsTwo($row2['item_id'], $row2['price'], $row2['item_images_json']);
						$products[] = $osi;
					}
					
				}

				for ($i=0; $i < count($products) ; $i++) { 
				$item = $products[$i];
				$salesId = $item->getItemId();
				$image = json_decode($item->getItemImagesJson());
			?>
			<div class="savedAd-box col-md-2 image-border-radius">
			<div class="ad-image-holder">
				<?php
					echo "<img src=\"./back/imageUpload/$image\" class=\"modify-ad-image\">";
				?>
			</div>
			<h3 class="ad-price">NRs. <?php echo $item->getPrice(); ?></h3>
			<?php
			echo "<button type=\"button\" onclick=\"showProductDetails('$salesId');\">Details</button> <br> <br>";
			echo "<button type=\"button\" onclick=\"removeSavedAd('$uId','$salesId');\">Remove</button> <br> <br>";
			?>

		</div>
<?php
	}
?>

			<script type="text/javascript" src= "./front/js/link.js"></script>
			<script type="text/javascript" src= "./front/js/home.js"></script>
			<script type="text/javascript" src= "./front/js/adDescription.js"></script>
			

	</body>

		
</html>