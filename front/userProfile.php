<!DOCTYPE html>
	<html>
		<head>
			<title>Index</title>


		</head>
		<body>
			<?php
				$sid = $_GET['sid'];
				require_once('./back/db/dbConnect.php');
				$dbConn = connectDb();
				$sql = "SELECT * FROM users WHERE user_id = '$sid'";
				$sql2 = "SELECT adv_title, item_images_json,accepted_order_id FROM on_sale_items WHERE sold_status = 'order_delivered'";
				$result = $dbConn->query($sql);
				$res=$result->fetch_assoc();
				$feedbacks = array();
				$ratings = array();
				$images = array();
				$adTitles = array();
				$buyerNames = array();
				$totalRatingSum = 0;
				$result2 = $dbConn->query($sql2);
				while($row = $result2->fetch_assoc()) {
					$accepted_order_id = $row['accepted_order_id'];
					array_push($images, json_decode($row['item_images_json']));
					array_push($adTitles, $row['adv_title']);
					$sql3 = "SELECT rating, feedback, f_name FROM orders INNER JOIN users on orders.buyer_id=users.user_id WHERE order_id = '$accepted_order_id'";
					$result3 = $dbConn->query($sql3);

					while($row3 = $result3->fetch_assoc()){
						array_push ($feedbacks, $row3['feedback']);
						array_push ($ratings, $row3['rating']);
						array_push($buyerNames, $row3['f_name']);
						$totalRatingSum += $row3['rating'];
					}
				}
			?>
			<div class="body-section col-md-12">
				<?php
					include('header.php');
				?>
				<div class="seller-profile-body">
					
					<div class="user-profile-container">
						<img src="./Images/user_icon.png" class="mdf-user-icon">
					</div>

					<h2>Average Rating:  <?php echo $totalRatingSum/count($ratings);?></h2>
					<div class="user-details-container">
						<label>First Name:  <?php echo $res['f_name']?></label><br>
						<label>Last Name:  <?php echo $res['l_name']?></label><br>
						<label>Address:  <?php echo $res['address']?></label><br>
					</div>
				</div>
				<div class="seller-review-container">
					<h1>Feedback Details</h1><br>
					<table>
						<?php
							for($i = 0; $i<count($ratings); $i++){
							?>
								<tr>
									<td>
										<div class="sellerPageAdImgContainer">
										  <img src="./back/imageUpload/<?php echo $images[$i]?>" class="sellerPageAdImg">
										</div>
									</td>
									<td>
										<div class="sellerReviewBox">
											Ad Title: <?php echo $adTitles[$i]; ?><br>
											Buyer Rating: <?php echo $ratings[$i]; ?><br>
											Buyer Feedback: <?php echo $feedbacks[$i];?><br>
											Buyer Name: <?php echo $buyerNames[$i];?><br>
										</div>
									</td>
								</tr>
							<?php
							}
						?>
					</table>
				</div>

		</div>
			
		</body>
	</html>


