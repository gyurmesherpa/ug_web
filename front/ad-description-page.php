
<!DOCTYPE html>
	<html>
		<head>
			<title>AD Description</title>
		</head>
		<body>
			<?php
				include('header.php');
			?>

			<?php
				require_once('./back/logics/buyerTasks.php');
				$bt = new BuyerTasks();
				$res = $bt->getAdDetails(AD_ID);
				$animal = $res->animal;

				//location co-ordinates:
				$latitude = "";
				$longitude = "";
				//issue_report_id	reporter_farm_id	animal	reporter_suspected_issue	reported_symptoms	expert_issue_confirmation	report_subject	reported_at	incident_of_or_since
				$farmId = $res->reporter_farm_id;
				$ud = $res->reporter_farm_id;
				$AdCount = $res->reporter_farm_id;

				$uId = USER_ID_AUTH;
				$adId = AD_ID;


			?>
			<div class="ad-description-container col-md-12">
				<?php
					echo "<input type=\"hidden\" id=\"adId\" name=\"\" value=\"".AD_ID."\">";
					echo "<input type=\"hidden\" id=\"sellerId\" name=\"\" value=\"".$farmId."\">";

				?>
				<div class="ad-images-container col-md-6">

					<?php
						 //$image = json_decode($osi->getItemImagesJson());
						 echo "<img src=\"./back/imageUpload/animals.jpeg\" class=\"modify-display-ad-image\">";
					?>
				</div>
				<div class="ad-short-description col-md-5 pull-right">
					<h1 class="ad-price"> <?php echo "Contact the farm for Details";//firmname ?></h1>
					<hr>
					<?php

					require_once('./back/db/dbConnect.php');
					$dbConn = connectDb();
					$sql = "select * from farm_details where farm_id = '".$farmId."'";
					$result = $dbConn->query($sql);
					while($row = $result->fetch_assoc()) {
						$latitude = $row['lat_cor'];
						$longitude = $row['lon_cor'];
						//farm_id	farm_email	farm_name	farm_pwd	farm_phone	farm_google_plus_code	district
					?>
					<p><b>Farm Name: </b><?php echo $row['farm_name']; ?> <br>
						<b>District: </b><?php echo $row['district']; ?> <br>
						<b>G Plus Code: </b><?php echo $row['farm_google_plus_code']; ?> <br>
						<b>Email: </b><?php echo $row['farm_email']; ?> <br>
						<b>Phone no: </b><?php echo $row['farm_phone']; ?> <br>
					</p>
				<?php } $dbConn->close();?>
					<hr>



				</div>

				<div class="comment-section col-md-6 pull-left">
					<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
					<link href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" rel="stylesheet"/>
					<div id="osm-map">
					</div>
					<script>
							// Where you want to render the map.
							var element = document.getElementById('osm-map');

							// Height has to be set. You can do this in CSS too.
							element.style = 'height:600px; margin-top: 10px';

							// Create Leaflet map on map element.
							var map = L.map(element);

							// Add OSM tile layer to the Leaflet map.
							L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
									attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							}).addTo(map);

							// Target's GPS coordinates.
							var target = L.latLng(<?php echo $latitude; ?> , <?php echo $longitude; ?> );

							// Set map's center to target with zoom 14.
							map.setView(target, 8);

							// Place a marker on the same location.
							L.marker(target).addTo(map);
					</script>

				</div>
				<div class="seller-info pull-right col-md-5">
					<h1 class="ad-price"> <?php echo "Reported issue details";//firmname ?></h1>
					<hr>
					<p><b>Animal: </b><?php echo $res->animal; ?> <br>
						<b>Subject: </b><?php echo $res->report_subject; ?> <br>
						<b>Suspected Issue: </b><?php echo $res->reporter_suspected_issue; ?> <br>
						<b>Reported Symptoms: </b><?php echo $res->reported_symptoms; ?> <br>

					</p>
					<hr>
					<h1>Confirm Disease/Issue</h1>
					<h3>Add to database, if you do not see in options.</h3>
					<br>
					<select id="diseasesFromDb">
						<?php
						//require_once('./back/db/dbConnect.php');
						$dbConn = connectDb();
						$sql = "select issue from animal_related_issues where animal = '".$animal."'";
						$result = $dbConn->query($sql);
						while($row = $result->fetch_assoc()) {
							//farm_id	farm_email	farm_name	farm_pwd	farm_phone	farm_google_plus_code	district
						?>
							<option> <?php echo $row['issue']; ?> </option>
					<?php } $dbConn->close();?>
					</select>
					<br>
					<input type="button" id="issueConfirmBtn" value="Confirm Disease/Issue" onclick="confirmAnimalIssue(<?php echo $adId; ?>)">
				</div>

			</div>
		</div>
<!-- Ad description Container Closing-->




			<script type="text/javascript" src= "./front/js/link.js"></script>
			<script type="text/javascript" src= "./front/js/adComment.js"></script>
			<script type="text/javascript" src= "./front/js/adDescription.js"></script>

		</body>



	</html>
