$( document ).ready(function() {
	$('.main-userMsgContainer').hide();
 	$('.msgText-conatiner').hide();	
	$('.buyerDetails').hide();
	$('.msg-textareaContainer').hide();
 	$('.msgText-conatiner').hide();
});

var selectedOrderId;
var selectedAdId;
function showMsgCount(){
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	var msgType = $('#msgType').val();
	if (msgType == "Buyer Messages") {
			$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, aKey:aKey, typeMsg: msgType, request:'showBuyerMsg' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if((data).trim() == "error")
				  {
					 alert("error");
				  }
				  else
				  {
 					 $('.msgText-conatiner').hide();
				  	 $('.main-userMsgContainer').empty();
				  	 $('.main-userMsgContainer').append(data);
					 $('.main-userMsgContainer').show();


				  }
			}
		});
	}
	else if(msgType == "Seller Messages"){
		$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, aKey:aKey, typeMsg: msgType, request:'showSellerMsg' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if((data).trim() == "error")
				  {
					 alert("error");
				  }
				  else
				  {
				  	//console.log(data);
 					 $('.msgText-conatiner').hide();
				  	 $('.main-userMsgContainer').empty();
				  	 $('.main-userMsgContainer').append(data);
					 $('.main-userMsgContainer').show();


				  }
			}
		});
	}
}

function showProductMsg(salesId){
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, aKey:aKey, salesId: salesId, request:'showProductMsg' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if((data).trim() == "error")
				  {
					 alert("error");
				  }
				  else
				  {
				  	//console.log(data);
					 $('.msg-textareaContainer').hide();

				  	 $('.main-userMsgContainer').hide();
				  	 $('.msgText-conatiner').empty();

				  	 $('.msgText-conatiner').append(data);	
 	 				 $('.msgText-conatiner').show();	

				  }
			}
		});
}

function acceptOrder(){
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	var orderMsg = $('#confirmOrderMsg').val();

	$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, orderMsg:orderMsg, aKey:aKey, adId: selectedAdId, orderId:selectedOrderId, request:'acceptOrder' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if(data.trim() == "Order_Accp")
				  {
					$('.msg-textareaContainer').hide();
					 showProductMsg(selectedAdId);
					 alert("Order Accpet Message Sent Sucessfully.");
				  }
				  else
				  {
					alert(data);
				  }
			}
		});
}

function showSellerMsg(adId){
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, aKey:aKey, salesId: adId, request:'showAcceptedSellerMsg' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if((data).trim() == "error")
				  {
					 alert("error");
				  }
				  else
				  {
				  	//console.log(data);
					 $('.msg-textareaContainer').hide();

				  	 $('.main-userMsgContainer').hide();
				  	 $('.msgText-conatiner').empty();

				  	 $('.msgText-conatiner').append(data);	
 	 				 $('.msgText-conatiner').show();
 					 $('.sellerRating').hide();	


				  }
			}
		});
}
function rateSeller(salesId){
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	var rating = $("input[name='sellerStarRating']:checked").val();
	var feedback = $('#sellerReviewTextBox').val();
	// if ($("input[name='sellerStarRating']").is(":checked")) {
 //   		console.log(rating);
	// }
	// else{
	// 	alert("required");
	// }
	if (feedback == "" || $("input[name='sellerStarRating']").is(":checked") == false) {
		alert("Alll field is required");
	}
	else{
		$.ajax({
				method: "POST",
				url: "",
				data: { auth_user_id: userId, aKey:aKey, adId:salesId, rating: rating, feedback:feedback, request:'rateSeller' }
			})

		.done(function( data, textStatus, jqXHR ) {
			if(textStatus =="success")
			{
			      if((data).trim() == "done")
				  {
				  	alert("Feedback Submitted Sucessfully");	
				  	 showMsgCount();
				  }
				  else
				  {
					 alert("error");	
				  }
			}
		});
	}

}

function displayOrderConfirmDialog(adId, orderId){
	$('.buyerMsgText').hide();
	$('.msg-textareaContainer').show();
	selectedAdId = adId;
	selectedOrderId = orderId;
}

function cancelOrderMsg(){
	$('.msg-textareaContainer').hide();
	$('.buyerMsgText').show();
}

function dealClosed(){
 	var priceNegotiable = $("input[name='dealClosed']:checked").val();
 	if (priceNegotiable == "Yes") {
 		$('.sellerRating').show();
 	}

}
