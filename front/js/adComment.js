$( document ).ready(function() {

});




function showComments(adId){
	$.ajax({
		method: "POST",
		url: "",
		data: { adId: adId, request:'showComments' }
	})
	.done(function( data, textStatus, jqXHR ) {
		if(textStatus =="success")
		{
		      if((data).trim() == "error")
			  {
				 alert("Couldn't Load");
			  }
			  else
			  {
			  	$('.comment-section').empty();
			  	$('.comment-section').append(data);
			  	//console.log(data);

			  }
		}
	});
}

function confirmAnimalIssue(issueReportId){
	var confirmedIssue = $("#diseasesFromDb option:selected").text();
	$.ajax({
		method: "POST",
		url: "",
		data: { issue_report_id: issueReportId, confirmed_issue: confirmedIssue, request:'confirm_issue' }
	})
	.done(function( data, textStatus, jqXHR ) {
		if(textStatus =="success")
		{
					if((data).trim() == "successfully updated")
				{
				 //alert("Successfully updated");
				 window.location.href="?request=home_page";
				}
				else
				{
					//alert("update failed");
					window.location.href="?request=home_page";

				}
		}
	});
}
