function commentSubmission(){
	var comment = $('#comment-text-box').val();
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	var adId = $('#adId').val();
	console.log(adId);
	if (userId == "" || aKey == "") {
		window.location.href="?request=register_page";
	}
	else{
			if(comment == ""){
				alert("Please Type a comment.");
			}
		else{
	     	$.ajax({
				method: "POST",
				url: "",
				data: { comment:comment, commenterId:userId, authKey:aKey, adId:adId, request:'commentSubmission'}
			})
			.done(function( data, textStatus, jqXHR) {
				if(textStatus === "success"){
					if((data).trim() == "comment_posted"){
						showComments(adId);
					}
				}
				else{
					alert(data);

				}
			});

		}
	}
}

function showReplySection(commentId){
	console.log("jbkjbkj");
	//$('#'+commentId).show();
	if($('#'+commentId).is(":visible")){
		$('#'+commentId).hide();
	}
	else{
		$('#'+commentId).show();
	}
}
function replyTo(commentId){
	alert("hello");
	var comment = $('#'+commentId+'-textarea').val();
	var userId = $('#userId').val();
	var aKey = $('#authKey').val();
	var adId = $('#adId').val();
	console.log(adId);
	if (userId == "" || aKey == "") {
		window.location.href="?request=register_page";
	}
	else{
			if(comment == ""){
				alert("Please Type a comment.");
			}
		else{
	     	$.ajax({
				method: "POST",
				url: "",
				data: { comment:comment, commenterId:userId, commentId:commentId, authKey:aKey, adId:adId, request:'replyComment'}
			})
			.done(function( data, textStatus, jqXHR) {
				if(textStatus === "success"){
					if((data).trim() == "comment_posted"){
						showComments(adId);
					}
				}
				else{
					alert(data);

				}
			});

		}
	}
}
$( document ).ready(function() {
		$('.msg-textareaContainer').hide();

});


function placeOrder(){
	var userId = $('#userId').val();
	var sId = $('#sellerId').val();
	if (userId == sId) {
		alert("Oops! You cannot order your own item.");
	}
	else{
		if($('.msg-textareaContainer').is(":hidden")){
			$('.msg-textareaContainer').show();
			$('#placeOrderBtn').hide();
		}
	}
}

function cancelOrder(){
	$('.msg-textareaContainer').hide();
	$('#placeOrderBtn').show();
}

function confirmOrder(){
	var orderMsg = $('#orderMsg').val();
	var uId = $('#userId').val();
	var adId = $('#adId').val();
	var aKey = $('#authKey').val();

	if (userId == "" || aKey == "") {
		window.location.href="?request=register_page";
	}
	else{
			if(orderMsg == ""){
				alert("Please Type a Message for seller");
			}
		else{
	     	$.ajax({
				method: "POST",
				url: "",
				data: { orderMsg:orderMsg, auth_user_id:uId, auth_user_key:aKey, adId:adId, request:'placeOrder'}
			})
			.done(function( data, textStatus, jqXHR) {
				if(textStatus === "success"){
					if((data).trim() == "done"){
						alert("Order Message Sent Sucessfully.");
						//In future when ad description page load. it must check whether
						//the user has already sent order msg or not to hide order btn
					}
					else{
						alert((data).trim());
					}
				}

			});

		}
	}
}

function sellerProfile(){
	var sellerId = $('#sellerId').val();
	alert(sellerId);
}

function saveAd(){
	var uId = $('#userId').val();
	var adId = $('#adId').val();
	var aKey = $('#authKey').val();
	if (userId == "" || aKey == "") {
		window.location.href="?request=register_page";
	}

	else{
	     	$.ajax({
				method: "POST",
				url: "",
				data: {  auth_user_id:uId, auth_user_key:aKey, adId:adId, request:'saveAd'}
			})
			.done(function( data, textStatus, jqXHR) {
				if(textStatus === "success"){
					if((data).trim() == "done"){
						alert("Ad saved Sucessfully");
						window.location.href="";

						//In future when ad description page load. it must check whether
						//the user has already sent order msg or not to hide order btn
					}
					else{
						alert((data).trim());
					}
				}

			});

	}
}

function removeSavedAd(uId,adId){
	console.log("ssssss");
	// var uId = $('#userId').val();
	// var adId = $('#adId').val();
	var aKey = $('#authKey').val();
	console.log(uId);
	console.log(adId);


	if (userId == "" || aKey == "") {
		window.location.href="?request=register_page";
	}

	else{
	     	$.ajax({
				method: "POST",
				url: "",
				data: {  auth_user_id:uId, auth_user_key:aKey, adId:adId, request:'removeSavedAd'}
			})
			.done(function( data, textStatus, jqXHR) {
				if(textStatus === "success"){
					if((data).trim() == "done"){
						alert("Ad Removed Sucessfully");
						window.location.href="";
						//In future when ad description page load. it must check whether
						//the user has already sent order msg or not to hide order btn
					}
					else{
						alert((data).trim());
					}
				}
			});

	}
}
