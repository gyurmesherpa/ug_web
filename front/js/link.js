

function buy(){
	$('#how-to-sell-text').hide();
	$('#safety-tips-text').hide();
	$('#how-to-buy-text').show();

}

function sell(){
	$('#how-to-buy-text').hide();
	$('#safety-tips-text').hide();
	$('#how-to-sell-text').show();
}

function safety(){
	$('#how-to-buy-text').hide();
	$('#how-to-sell-text').hide();
	$('#safety-tips-text').show();
}


function userRegister(){
	var first_name = $("#user_first_name").val();
	var last_name = $("#user_last_name").val();
	var email = $("#user_email").val();
	var password = $("#user_password").val();
	var confirm_password = $("#user_confirm_password").val();
	var contact_number = $("#user_contact").val();
	var address = $("#user_address").val();
	var regEx = /\S+@\S+\.\S+/;

	if (!first_name || !last_name || !email || !password || !confirm_password || !contact_number || !address)
	    {
	     alert("All Fields are Required.")
	    }
	    if(password!=confirm_password)
	    {
	    	alert("Password Does not match")
	    }

	    var validEmail = regEx.test(email);
	     if (!validEmail)
	      {
	      	alert("Pleae Enter Valid Email");
	     }
	     else{
		     	$.ajax({
							method: "POST",
							url: "",
							data: { user_first_name: first_name, user_last_name: last_name, user_email: email,
								  user_password: password, user_contact_number:contact_number, user_address:address,
								  request:'register_new_user'}
					})
						.done(function( data, textStatus, jqXHR) {
							console.log("ajax_status: " + textStatus + " resonse-data.lenth: " + data.trim().length);
							if(textStatus === "success"){
								alert((data).trim());
							}
							else{
								alert(data);
							}
						});
	     }
}


function userLogin(){
	var email = $("#user_signin_email").val();
	var password = $("#user_signin_password").val();
	var regEx = /\S+@\S+\.\S+/;
	console.log(email);
	var validEmail = regEx.test(email);
	 if (!validEmail){
	    	alert("Please Enter Valid Email");
	 }

	 else {
			if (email=="" || password=="")
   				 {
					alert("Please enter your email and password");
				 }

				else{
						$.ajax({
							method: "POST",
							url: "",
							data: { user_email: email, user_password: password,request:'user_login' }
						})
						.done(function( data, textStatus, jqXHR ) {
							if(textStatus =="success")
							{
							      if((data).trim() == "error")
								  {
					                alert("Email or Password does not matched.\n Please Try Again.");
								  }
								  else
								  {
								    var authDetails = JSON.parse(data);
										//alert(data);
	 							     window.location.href="?request=home_page&&auth_user_id="+authDetails.user_id+"&&auth_user_key="+authDetails.auth_key;
								  }
							}
						});
				}
	      }
 }



 function setAuthKeys(uId, authKey){
	$('#userId').val(uId);
	$('#authKey').val(authKey);
 }

 function toShowBrand(){
 	var category = $('#category-type').val();
 	if (category == "Mobile") {
 		$("#brand-name").empty();
 		mobileBrand();
 	}
 	if (category == "Motorcycle") {
 		$("#brand-name").empty();
 		motorcycleBrand();
 	}
 	if (category == "Car") {
 		$("#brand-name").empty();
 		carBrand();
 	}
 	if (category == "Scooter") {
 		$("#brand-name").empty();
 		scooterBrand();
 	}
 	var brand = $('#brand-name').val();
 	console.log(category);
 	console.log(brand);
 }

 function mobileBrand(){
 	$("#brand-name").append(new Option("LG", "LG"));
 	$("#brand-name").append(new Option("Redimi", "Redimi"));
 	$("#brand-name").append(new Option("OnePlus", "OnePlus"));
 	$("#brand-name").append(new Option("Pixel", "Pixel"));
 	$("#brand-name").append(new Option("Iphone", "Iphone"));
 	$("#brand-name").append(new Option("MI", "MI"));
 	$("#brand-name").append(new Option("OPPO", "OPPO"));
 	$("#brand-name").append(new Option("VIVO", "VIVO"));
 	$("#brand-name").append(new Option("Huawei", "Huawei"));
 }
 function motorcycleBrand(){
 	$("#brand-name").append(new Option("Pulsar", "pulsar"));
 	$("#brand-name").append(new Option("Apache", "Apache"));
 	$("#brand-name").append(new Option("R15", "R15"));
 	$("#brand-name").append(new Option("Hunk", "Hunk"));
 	$("#brand-name").append(new Option("Unicorn", "Unicorn"));
 	$("#brand-name").append(new Option("KTM Duke", "KTM Duke"));
 	$("#brand-name").append(new Option("RC", "RC"));
 	$("#brand-name").append(new Option("Royal Enfield", "Royal Enfield"));
 	$("#brand-name").append(new Option("Splender", "Splender"));
 	$("#brand-name").append(new Option("Beneli", "Beneli"));
 }
 function carBrand(){
 	$("#brand-name").append(new Option("Alto", "Alto"));
 	$("#brand-name").append(new Option("Bolero", "Bolero"));
 	$("#brand-name").append(new Option("Scorpio", "Scorpio"));
 	$("#brand-name").append(new Option("Swift Dzire", "Swift Dzire"));
 	$("#brand-name").append(new Option("Alto", "Alto"));
 	$("#brand-name").append(new Option("Mazda", "Mazda"));
 }

 function scooterBrand(){
 	$("#brand-name").append(new Option("Dio", "Dio"));
 	$("#brand-name").append(new Option("Vespa", "Vespa"));
 	$("#brand-name").append(new Option("Pleasure", "Pleasure"));
 	$("#brand-name").append(new Option("Jupiter", "Jupiter"));
 	$("#brand-name").append(new Option("Aviator", "Aviator"));
 }



 function adSubmission(){
 	var seller_id = $("#userId").val();
 	var auth_key = $("#authKey").val();
 	var ad_title = $("#ad-title").val(); //issueName or disease
 	var product_type = $('#category-type').val(); //animal
 	var product_brand = $('#brand-name').val();
 	var product_description = $('#product-description').val(); //Symptoms
 	var priceNegotiable = $("input[name='priceNegotiable']:checked").val();
 	var product_price = $('#product-price').val();
 	var photo_one = $('#product-photo-one')
 	var photo_two = $('#product-photo-two')
 	var photo_three = $('#product-photo-three');
 	var photo_four = $('#product-photo-four');

 	if (!ad_title || !product_type || !product_description) {
 		alert("All Fileds are Necessary including product photos");
 	}
 	else{
 		//console.log(ad_title+product_type+product_brand+product_description+priceNegotiable+product_price);

 	    	 $.ajax({
			  		 method: "POST",
			  		 url: "",
			  		 data: { check_session:product_description, auth_user_id:seller_id, auth_user_key:auth_key, ad_title: ad_title, product_type: product_type, product_brand: product_brand, product_description: product_description, price_negotiable: priceNegotiable, product_price: product_price, request:'adv_submission' }
			 })
			 .done(function( data, textStatus, jqXHR ) {
			  		 if(textStatus =="success"){
			  		 	if(data.trim() == "Successfully added"){
								window.location.href="?request=home_page";
							}
							else{
								alert(data);
							}
					 }

		     });


 	};
}

function imageSubmission(adId){
	var fd = new FormData();
    var files = $('#file1')[0].files;

        // Check file selected or not
        if(files.length > 0 ){
           fd.append('file',files[0]);
           fd.append('ad_id', adId);
           $.ajax({
              url: './back/imageUpload/index.php',
              type: 'post',
              data: fd,
              contentType: false,
              processData: false,
              success: function(response){
                 if(response != 0){
                    $("#img").attr("src",response);
                    $(".preview img").show(); // Display image element
                    alert(response);
	 				 window.location.href="";

                 }else{
                    alert(response);
                 }
              },
           });
        }else{
           alert("Please select a file.");
        }
}

//Testing For gits
function sellPage(){
	//get user id and auth from hidden field
	var userId = $('#userId').val();
	var authKey = $('#authKey').val();
	//the following line didn't support || authKey == "" condition
	if(userId == ""){
	   window.location.href="?request=register_page";
	}
	else{
		//sent request with request = sell_page, username =; authKey =;
		window.location.href="?request=sell_page&&auth_user_id="+userId+"&&auth_user_key="+authKey;
	}

	//if they are empty prompt login
	//if not propmt take to sell page(In the new page the Uid and authkey will put by server backend)

}
function homePage(){
	var userId = $('#userId').val();
	var authKey = $('#authKey').val();
	window.location.href="?request=unset&&auth_user_id="+userId+"&&auth_user_key="+authKey;
}
function faqPage(){
	var userId = $('#userId').val();
	var authKey = $('#authKey').val();

	window.location.href="?request=faq_page&&auth_user_id="+userId+"&&auth_user_key="+authKey;
}
function adDisplayPage(){
	var userId = $('#userId').val();
	var authKey = $('#authKey').val();
	window.location.href="?request=adDisplay_page&&auth_user_id="+userId+"&&auth_user_key="+authKey;
}

function msgPage(){
	var userId = $('#userId').val();
	var authKey = $('#authKey').val();
	window.location.href="?request=user_messagePage&&auth_user_id="+userId+"&&auth_user_key="+authKey;

}

function getImageToUpload(id){
	if(id == 'product-photo-one'){
		$('#file1').change(function(){
		    var input = this;
		    var url = $(this).val();
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
		     {
		        var reader = new FileReader();

		        reader.onload = function (e) {
							$('#'+id).empty();
							$('#'+id).append("<img src=\"\" class=\"upload-image-display\" id=\"product-photo-1\">");
		           $('#img').attr('src', e.target.result);
							 $('#product-photo-1').attr('src', e.target.result);
		        }
		       reader.readAsDataURL(input.files[0]);
		    }
		    else
		    {
		      $('#img').attr('src', '/assets/no_preview.png');
		    }
		  });

		$("#file1").trigger("click");
	}
	else if (id == 'product-photo-two') {
		$('#file2').change(function(){
		    var input = this;
		    var url = $(this).val();
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
		     {
		        var reader = new FileReader();

		        reader.onload = function (e) {
							$('#'+id).empty();
							$('#'+id).append("<img src=\"\" class=\"upload-image-display\" id=\"product-photo-2\">");
		           $('#img').attr('src', e.target.result);
							 $('#product-photo-2').attr('src', e.target.result);
		        }
		       reader.readAsDataURL(input.files[0]);
		    }
		    else
		    {
		      $('#img').attr('src', '/assets/no_preview.png');
		    }
		  });

		$("#file2").trigger("click");
	}
	else if (id == 'product-photo-three') {
		$('#file3').change(function(){
		    var input = this;
		    var url = $(this).val();
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
		     {
		        var reader = new FileReader();

		        reader.onload = function (e) {
							$('#'+id).empty();
							$('#'+id).append("<img src=\"\" class=\"upload-image-display\" id=\"product-photo-3\">");
		           $('#img').attr('src', e.target.result);
							 $('#product-photo-3').attr('src', e.target.result);
		        }
		       reader.readAsDataURL(input.files[0]);
		    }
		    else
		    {
		      $('#img').attr('src', '/assets/no_preview.png');
		    }
		  });

		$("#file3").trigger("click");
	}
	else if (id == 'product-photo-four') {
		$('#file4').change(function(){
		    var input = this;
		    var url = $(this).val();
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
		     {
		        var reader = new FileReader();

		        reader.onload = function (e) {
							$('#'+id).empty();
							$('#'+id).append("<img src=\"\" class=\"upload-image-display\" id=\"product-photo-4\">");
		           $('#img').attr('src', e.target.result);
							 $('#product-photo-4').attr('src', e.target.result);
		        }
		       reader.readAsDataURL(input.files[0]);
		    }
		    else
		    {
		      $('#img').attr('src', '/assets/no_preview.png');
		    }
		  });

		$("#file4").trigger("click");
	}
	else{
		alert("error");
	}
}

function displayImageToUpload(input){
	//alert(input.value);
	//document.getElementById("img").src = input.value;
}
//
// $('#file1').change(function(){
//     var input = this;
//     var url = $(this).val();
//     var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
//     if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
//      {
//         var reader = new FileReader();
//
//         reader.onload = function (e) {
//            $('#img').attr('src', e.target.result);
//         }
//        reader.readAsDataURL(input.files[0]);
//     }
//     else
//     {
//       $('#img').attr('src', '/assets/no_preview.png');
//     }
//   });
