<?php
  class SellerTasks
  {
    //note: the use of term var for properties (member variables) are
    //depracated since v5 but still backward compatible.
    //v7 and later supports data-type declaration (eg int $i)
    private $seller; //object of class User
    private $onSaleItem; //object of class OnSaleItem
    private $buyer; //object of User
    private $purchase; //object of Purchase

    //add item to sales list
    function addToSales($productDetails){
      require_once('./back/db/sellerDb.php');
      $sellerDb = new SellerDb();
      return $sellerDb->addItemToSales($productDetails); //true-false
    }

    //remove from sales
    function removeFromSales($itemId){

    }

    //getters and setters
    public function getAllCategories(){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/sellerDb.php');
      $sdb = new SellerDb();
      return $sdb->getItemCategories();
    }
    public function getAdDetails($adId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/sellerDb.php');
      $sdb = new SellerDb();
      return $sdb->getAdDetails($adId); 
    }
    public function replyComment($comment_id,$datetime, $comment, $item_id, $comment_type_id, $commenter, $reply_to){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/sellerDb.php');
      $sdb = new SellerDb();
      return $sdb->postCommentReply($comment_id,$datetime, $comment, $item_id, $comment_type_id, $commenter, $reply_to); 
    }
    public function acceptOrder($adId, $orderId, $orderMsg){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/sellerDb.php');
      $sdb = new SellerDb();
      return $sdb->acceptOrder($adId, $orderId, $orderMsg); 
    }

    public function getSellerDetails(){
      
    }
    public function getSeller()
    {
      return $this->seller;
    }
    public function setSeller($seller)
    {
      $this->seller = $seller;
    }
    public function getOnSaleItem()
    {
      return $this->onSaleItem;
    }
    public function setOnSaleItem($onSaleItem)
    {
      $this->onSaleItem = $onSaleItem;
    }
    public function getBuyer()
    {
      return $this->buyer;
    }
    public function setBuyer($buyer)
    {
      $this->buyer = $buyer;
    }
    public function getPurchase()
    {
      return $this->purchase;
    }
    public function setPurchase($purchase)
    {
      $this->purchase = $purchase;
    }
}

?>
