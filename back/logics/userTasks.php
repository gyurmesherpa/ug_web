<?php


  class UserTasks
  {
    //register new user
    function registerNewUser($newUser){
      include './back/db/userDb.php';
      $db = new UserDB();
      return $db->registerNewUser($newUser);
    }

    //update user details
    function updateUserDetails(){

    }

    //get all user details by user_id
    function getUserDetails($userEmail){
      include './back/db/userDb.php';
      $db = new UserDB();
      return $db->getUserDetails($userEmail);
    }
  }

?>
