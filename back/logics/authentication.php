<?php
  include './back/logics/hashing.php';
  class Authenticator
  {
    //private $userId;//userID means Email here in this case
    private $reqTime; //the $_SERVER['REQUEST_TIME'] at login backend
    private $authKey;
    private $userDetails;
     //key generated upon successful login and to be stored
    //both in session and front end cache for authentication
    function __construct($userDetails, $reqTime)
    {
      $this->userDetails = $userDetails;
      $this->reqTime = $reqTime;

    }
    public function pwdMatchCheck($sentPwd){
     //include './back/db/userDb.php';
     //$db = new UserDb();
      $authMsg;
      //get hashed pwd from db;
      //for now connecting to db from here not required because, it has been prior fetched in index
      $hashedPwdFromDb = $this->userDetails->getPassword();
      //hash the user provided pwd
      $hashing = new Hashing($sentPwd);
      $hashedPwdSent = $hashing->getPwdHash();
      //echo "from db:\n". $hashedPwdFromDb . "\nsent hashed:\n" . $hashedPwdSent;
      //compare the $hashes
      if(trim($hashedPwdSent)==trim($hashedPwdFromDb)){
        $authMsg = "match";
      }
      else{
        $authMsg = $hashedPwdSent;
      }
      return $authMsg;
    }
    public function generateAuthKey(){
      //generate logged-in-hash
      $hashing = new Hashing($this->reqTime);
      $this->authKey = $hashing->generateAuthKey();
      if (session_status() == PHP_SESSION_NONE) {
        session_start();
      }
      $sesName = $this->userDetails->getUserId();
      $authKey = $this->authKey;
      $reqTime = $this->reqTime;
      //store the $authKey, login-time (ie $reqTime to )
      $sessionVal = array("auth_key"=>"$authKey", "last_req_time"=>"$reqTime");
      $_SESSION["$sesName"] = $sessionVal;
      return $_SESSION["$sesName"]["auth_key"];
    }

    //the getters
    public function getUserId()
    {
      return $this->userDetails->getUserId();
    }
    public function getReqTime()
    {
      return $this->reqTime;
    }
    public function getAuthKey()
    {
      return $this->authKey;
    }
    public function trialSession(){
      if (session_status() == PHP_SESSION_NONE) {
        session_start();
      }
    }
    public function authenticate($sentAuth_key, $currentReqTime){
      $authResponse ="Default";
      $minutesBeforeSessionExpire=30;
      if(!isset($_SESSION[$this->userDetails->getUserId()])){
        $authResponse = "Session Not Set";
      }
      else{
        if($sentAuth_key == $_SESSION[$this->userDetails->getUserId()]['auth_key']){
          if(($currentReqTime - $_SESSION[$this->userDetails->getUserId()]['last_req_time']) < ($minutesBeforeSessionExpire*60)){
            $sesName = $this->userDetails->getUserId();
            $authKey = $_SESSION[$this->userDetails->getUserId()]['auth_key'];
            $reqTime = $currentReqTime;
            $sessionVal = array("auth_key"=>"$authKey", "last_req_time"=>"$reqTime");
            $_SESSION["$sesName"] = $sessionVal;
            $authResponse = "All Approved";
          }
          else{
            $authResponse = "Session Expired";
          }
        }
        else{
          $authResponse = "Wrong Auth Key Sent";
        }
      }
      return $authResponse;
    }
    public function updateLastRequestTime(){
      session_start();
      $_SESSION[$this->userDetails->getUserId()] = ["auth_key"=>$this->authKey, "last_req_time"=>$this->reqTime];
    }
  }
?>
