<?php
  class BuyerTasks
  {
    private $purchase; //the object of class Purchase
    // function __construct($purchase)
    // {
    //   $this->purchase = $purchase;
    // }
    public function getAdDetails($adId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/sellerDb.php');
      $sdb = new SellerDb();
      return $sdb->getAdDetails($adId);
    }
    function registerBid(){

    }
    //Comment type ID should be either reply or comment. 
    public function postComment($comment_id,$datetime, $comment, $item_id, $comment_type_id, $commenter){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $sdb = new BuyerDb();
      return $sdb->postComment($comment_id, $datetime, $comment, $item_id, $comment_type_id, $commenter);
    }

    public function getSellerDetails($uId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->getSellerDetails($uId); 
    }

    public function getAdCount($uId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->getAdCount($uId); 
    }

    public function placeOrder($orderId, $orderDate, $orderMsg, $uId, $adId, $status){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->placeOrder($orderId, $orderDate, $orderMsg, $uId, $adId, $status); 
    }
    public function rateSeller($adId,$rating,$feedback){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->rateSeller($adId,$rating,$feedback); 
    }
    public function checkSavedAd($adId, $uId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->checkSavedAd($adId, $uId);
    }
    public function saveAd($adId, $uId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->saveAd($adId, $uId); 
    }
    public function removeSavedAd($adId, $uId){
      require_once('./back/db/dbConnect.php');
      require_once('./back/db/buyerDb.php');
      $bdb = new BuyerDb();
      return $bdb->removeSavedAd($adId, $uId); 
    }


    

    function getAllPurchases(){

    }
    function cancelOrder(){

    }
  }

?>
