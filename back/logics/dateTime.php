<?php
  class CustomDateTime
  {
    private $dateTimeObj; //set to DEFAULT_TIMEZONE (set by user instead of utc)
    function __construct()
    {
      $dateTime = new DateTime();
      $dateTime->setTimeZone(new DateTimeZone(DEFAULT_TIMEZONE));
      $this->dateTimeObj = $dateTime;
    }
    public function getDateTimeStamp(){
      return $this->dateTimeObj->getTimestamp();
    }
    public function getMySqlTimeStamp(){
      return date('Y-m-d H:i:s',$this->dateTimeObj->getTimestamp());
    }
  }

?>
