<?php
   if(isset($_FILES['file']['name'])){

   /* Getting file name */
   $filename = $_FILES['file']['name'];
   /* Location */
   $location = "advertiseImage/".$filename;
   $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
   $imageFileType = strtolower($imageFileType);

   /* Valid extensions */
   $valid_extensions = array("jpg","jpeg","png");
   $destination = 'advertiseImage/'.$_POST['ad_id'].'.'.$imageFileType;
   $response = 0;
   /* Check file extension */
   if(in_array(strtolower($imageFileType), $valid_extensions)) {
      /* Upload file */
      if(move_uploaded_file($_FILES['file']['tmp_name'],$destination)){
         //json_encode($destination);
         require_once('../db/sellerDb.php');
         require_once('../db/dbConnect.php');
         $sdb = new SellerDb();
         if($sdb->addImageJson($_POST['ad_id'], json_encode($destination))){

            $response = "Success";
         }
      }
   }
   //$adId = $_POST['ad_id'];

   echo $response;
   exit;
}

echo 0;

?>