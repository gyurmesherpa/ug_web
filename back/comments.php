<h3>Comments</h3>
<hr>

<?php
	require_once('./back/db/buyerDb.php');
	$bdb = new BuyerDb();
	$sellerId = $bdb->getSellerId(AD_ID); 
	$comments = $bdb->getComments(AD_ID, "comment", "");

	for ($i=0; $i < count($comments); $i++) { 
		$com = $comments[$i];
?>

		<div class="retrieve-comment">
			<div class="main-comment" id="<?php echo $com['comment_id']; ?>-section">
				<span class="commenter-name-mdf"><b><?php echo $com['commenter_name']; echo ":  "; ?></b></span>
				<?php echo $com['comment']; ?>
				<?php
					$commentId = $com['comment_id'];
					if (AUTH_KEY != "" && USER_ID_AUTH == $sellerId) {
						echo "<input type=\"button\" name=\"\" value=\"Reply\" class=\"cmt-reply-btn\" onclick=\"showReplySection($commentId);\">";
					}
				?>
				<div class="show-reply-comments">
					<?php
						$replies = $bdb->getCommentReplies(AD_ID, 'reply', $commentId);
						//print_r($replies);
						for ($j=0; $j < count($replies); $j++) { 
							$reply = $replies[$j];
							?>
							<div class="retrieve-reply-section">
								<span class="commenter-name-mdf"><b>Seller:  </b></span><?php echo  $reply['comment']; ?>
							</div>
							<?php
						}
					?>
				</div>
				<div class="reply-section" id="<?php echo $commentId; ?>">
					<textarea rows="2" class="reply-mdf" id="<?php echo $commentId; ?>-textarea"></textarea>
					<input type="button" name="" value="Submit Reply" class="btn btn-primary modify-btn" onclick="replyTo(<?php echo $commentId; ?>);">
				</div>

				<script type="text/javascript">
					$('#<?php echo $commentId;?>').hide();
				</script>
			</div>
		</div>

<?php
	}
?>
				<textarea rows="2" id="comment-text-box"></textarea>
			<input type="button" name="" value="Comment" class="btn btn-primary modify-btn" onclick="commentSubmission();">
