<?php

  class SellerDb
  {
    function addItemToSales($itemDetails){

      $sellerId = $itemDetails->getSellerId();
      $itemId = $itemDetails->getItemId();
  		$itemAddedDate = $itemDetails->getItemAddedDate();
  		$advTitle = $itemDetails->getAdvTitle();
      //$itemImagesJson = $itemDetails->getItemImagesJson();
  		$category	= $itemDetails->getCategory();
  		$brandName = $itemDetails->getBrandName();
  		$description = $itemDetails->getDescription();
      $price = $itemDetails->getPrice();
      $priceNegotiability = $itemDetails->getPriceNegotiability();
      require_once('./back/db/dbConnect.php');
      $dbConn = connectDb();
 	    $sql = "INSERT INTO on_sale_items(item_id,adv_title, price, date_added, seller, category_id,price_negotiability, brand_name, description) VALUES ('$itemId','$advTitle', '$price', '$itemAddedDate', '$sellerId', '$category', '$priceNegotiability', '$brandName', '$description')";
        //$stmt->bind_param("sssssss", $user_id,$firstname, $lastname, $email, $password, $contactNo, $addressOne);
	    $success = $dbConn->query($sql);
      $dbConn->close();

      return $success;
    }

    function addImageJson($adId, $imageJson){
      $dbConn = connectDb();
      $sql = "UPDATE on_sale_items SET item_images_json = '$imageJson' WHERE item_id = '$adId'";
      $success = $dbConn->query($sql);
      $dbConn->close();
      return $success;
    }

    function getItemCategories(){
      $dbConn = connectDb();
      $sql = "SELECT * FROM animals";
      $result = $dbConn->query($sql);
      $categories =[];
      while($category = $result->fetch_assoc()) {
        $categories[]=$category;
      }
      $dbConn->close();
      return $categories;
    }

    function getAdDetails($adId){
      $dbConn = connectDb();
      $sql = "SELECT * FROM `reported_animal_related_issues` WHERE issue_report_id ='$adId'";
      $result = $dbConn->query($sql);

      $result = $dbConn->query($sql);
      $res=$result->fetch_object();
      // $osi->setDetails($res->seller, $res->date_added, $res->adv_title, $res->category_id, $res->brand_name,$res->description, $res->price_negotiability, $res->price);
      // $osi->setItemId($adId);
      // $osi->setItemImagesJson($res->item_images_json);

      $dbConn->close();
      return $res;
    }

    public function postCommentReply($comment_id,$datetime, $comment, $item_id, $comment_type_id, $commenter, $reply_to){
      $dbConn = connectDb();
        $sql = "INSERT INTO item_comments (comment_id,datetime, comment, item_id, comment_type_id, commenter, commenter_name, reply_to) VALUES ('$comment_id', '$datetime', '$comment', '$item_id', '$comment_type_id', '$commenter', (SELECT f_name FROM users WHERE user_id ='$commenter'), '$reply_to')";
        $success = $dbConn->query($sql);
          $dbConn->close();
          return $success;
    }

    public function acceptOrder($adId, $orderId, $orderMsg){
      $dbConn = connectDb();
      $sql = "UPDATE on_sale_items SET accepted_order_id = '$orderId', sold_status = 'order_accepted' WHERE item_id = '$adId'";
      $sql2 = "UPDATE orders SET order_acceptence_msg = '$orderMsg' WHERE order_id = '$orderId'";
      $success = $dbConn->query($sql);
      if ($success) {
        $sucess = $dbConn->query($sql2);
      }
      $dbConn->close();
      return $success;
    }

    // public function getSellerDetails(){
    //   $dbConn = connectDb();
    //   $sql = "SELECT * FROM ";
    //   $success = $dbConn->query($sql);
    //   $dbConn->close();
    //   return $success;
    // }
  }

?>
