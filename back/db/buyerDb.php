<?php
	class BuyerDb {

		public function __construct(){
    		require_once('./back/db/dbConnect.php');
		}
		public function getItemsBy($option, $Price_Direction, $date_direction){
			$dbConn = connectDb();
			$sql = "";
			if ($option == "recents") {
				$sql ="SELECT * FROM reported_animal_related_issues ORDER BY incident_of_or_since ASC;";
			}

			//echo $sql;

			$result = $dbConn->query($sql);
			//$res=$result->fetch_object();
			//include './back/models/onSaleItem.php';
			$issues=array();
			return $result;
		}

		public function postComment($comment_id,$datetime, $comment, $item_id, $comment_type_id, $commenter){
			//require_once('./back/db/dbConnect.php');
			$dbConn = connectDb();
	 	    $sql = "INSERT INTO item_comments (comment_id,datetime, comment, item_id, comment_type_id, commenter, commenter_name) VALUES ('$comment_id', '$datetime', '$comment', '$item_id', '$comment_type_id', '$commenter', (SELECT f_name FROM users WHERE user_id ='$commenter'))";
		    $success = $dbConn->query($sql);
	        $dbConn->close();
	        return $success;
		}

		public function getSellerId($pid){
			$dbConn = connectDb();
			$sql = "SELECT seller FROM on_sale_items where item_id = '"."$pid'";
			$result = $dbConn->query($sql);
			$res=$result->fetch_object();
			$sid= "";
			if($res){
				$sid=$res->seller;
			}else{
				$sid="error";
			}
			$dbConn->close();
			return $sid;
		}




		public function getComments($adId,$commentTypeId,$commentId){
			$dbConn = connectDb();
			$sql = "";
			if ($commentId == "") {
				$sql = "SELECT * from item_comments where (item_id = '$adId' AND comment_type_id = '$commentTypeId');";
			}
			else{
				$sql = "SELECT * from item_comments where (item_id = '$adId' AND comment_type_id = '$commentTypeId' AND reply_to = '$commentId');";
			}
	 	    $result = $dbConn->query($sql);
			//$res=$result->fetch_object();
			$comments = array();
			while($row = $result->fetch_assoc()) {
				$comments[] = $row;
			}
			//echo $sql;
			return $comments;
		}

		public function getCommentReplies($adId,$commentTypeId,$commentId){
			$dbConn = connectDb();
				$sql = "SELECT * from item_comments where (item_id = '$adId' AND comment_type_id = '$commentTypeId' AND reply_to = '$commentId');";
	 	    $result = $dbConn->query($sql);
			$res=$result->fetch_object();
			$comments = array();
			while($row = $result->fetch_assoc()) {
				$comments[] = $row;
			}
			//echo $sql;
			//print_r($res);
			return $comments;
		}

		public function getSellerDetails($uId){
    	require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "SELECT * FROM users WHERE user_id = '"."$uId'";
		$result = $dbConn->query($sql);
		$res=$result->fetch_object();
		include './back/models/user.php';
		$userDetails = new User($res->f_name, $res->l_name, $res->primary_email, $res->password, $res->address, $res->contact_no);
		$user_id = $res->user_id;
		$userDetails->setUserId($user_id);
		return $userDetails;
	}
	public function getAdCount($uId){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "SELECT COUNT(seller) AS 'seller' FROM on_sale_items  WHERE seller = '"."$uId'";
		$result = $dbConn->query($sql);
		$res=$result->fetch_object();
		$salesCount= "";
		if($res){
			$salesCount=$res->seller;
		}else{
			$salesCount="error";
		}
		$dbConn->close();
		return $salesCount;

	}
	public function placeOrder($orderId, $orderDate, $orderMsg, $uId, $adId, $status){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql0 = "SELECT * FROM orders WHERE buyer_id = '"."$uId' AND ad_id = '$adId'";
		$result0 = $dbConn->query($sql0);
		$rows_count = $result0->num_rows;
		if ($rows_count == 0) {
			$sql = "INSERT INTO orders (order_id, date, order_msg, buyer_id, ad_id ) VALUES ('$orderId', '$orderDate', '$orderMsg', '$uId', '$adId')";
			$sql2 = "UPDATE on_sale_items SET sold_status = '$status' WHERE item_id ='$adId'";
			$success = $dbConn->query($sql);
			if ($success) {
				$success = $dbConn->query($sql2);
				if($success){
					$success= "done";
				}
				else{
					$success = "error";
				}
			}
	    	$dbConn->close();
		}
		else{
			$success = "You have Already Placed an Order ";
		}
	   	return $success;
	}

	public function rateSeller($adId,$rating,$feedback){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "UPDATE orders SET rating ='$rating',feedback = '$feedback' WHERE ad_id = '$adId'";
		$sql2 = "UPDATE on_sale_items SET sold_status = 'order_delivered' WHERE item_id = '$adId'";
		$success = $dbConn->query($sql);
		if ($success) {
			$success = $dbConn->query($sql2);
		}
	    $dbConn->close();
	    return $success;
	}
	public function checkSavedAd($adId, $uId){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "SELECT id FROM saved_ads WHERE uid = '$uId' AND ad_id = '$adId'";
		$result = $dbConn->query($sql);
		$res=$result->fetch_object();
		$id= "";
		if($res){
			$id=$res->id;
		}else{
			$id = 0;
		}
		$dbConn->close();
		return $id;
	}

	public function saveAd($adId, $uId){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "INSERT INTO saved_ads (uid,ad_id) VALUES('$uId','$adId')";
		$success = $dbConn->query($sql);
	    $dbConn->close();
	    return $success;
	}

	public function removeSavedAd($adId, $uId){
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$sql = "DELETE FROM saved_ads WHERE ad_id = '$adId' AND uid = '$uId'";
		$success = $dbConn->query($sql);
	    $dbConn->close();
	    return $success;
	}



}
?>
