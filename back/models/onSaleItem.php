<?php

  class OnSaleItem
  {
    private $sellerId;
    private $itemAddedDate;
    private $item_id;
    private $advTitle; //item name
    private $itemImagesJson;
    private $category;
    private $brandName;
    private $description;
    private $price;
    private $priceNegotiability;
    private $soldStatus;
    private $soldDate;

    public function setDetails($sellerId, $itemAddedDate, $advTitle, $category, $brandName, $description, $priceNegotiability, $price){
      $this->sellerId = $sellerId;
      $this->itemAddedDate = $itemAddedDate;
      $this->price = $price;
      //$this->item_id = $item_id;
      $this->advTitle = $advTitle;
      $this->category = $category;
      $this->brandName = $brandName;
      $this->description = $description;
      $this->priceNegotiability = $priceNegotiability;
    }
    
    public function getDetailsTwo($item_id, $price, $itemImagesJson){
      $this->item_id = $item_id;
      $this->price = $price;
      $this->itemImagesJson = $itemImagesJson;
    }
    public function getSellerId()
    {
      return $this->sellerId;
    }
    public function setSellerId($sellerId)
    {
      $this->sellerId = $sellerId;
    }
    public function getItemAddedDate()
    {
      return $this->itemAddedDate;
    }
    public function setItemAddedDate($itemAddedDate)
    {
      $this->itemAddedDate = $itemAddedDate;
    }
    public function getItemId()
    {
      return $this->item_id;
    }
    public function setItemId($item_id)
    {
      $this->item_id = $item_id;
    }
    public function getAdvTitle()
    {
      return $this->advTitle;
    }
    public function setAdvTitle($advTitle)
    {
      $this->advTitle = $advTitle;
    }
    public function getItemImagesJson()
    {
      return $this->itemImagesJson;
    }
    public function setItemImagesJson($itemImagesJson)
    {
      $this->itemImagesJson = $itemImagesJson;
    }
    public function getCategory()
    {
      return $this->category;
    }
    public function setCategory($category)
    {
      $this->category = $category;
    }
    public function getBrandName()
    {
      return $this->brandName;
    }
    public function setBrandName($brandName)
    {
      $this->brandName = $brandName;
    }
    public function getDescription()
    {
      return $this->description;
    }
    public function setDescription($description)
    {
      $this->description = $description;
    }
    public function getPrice()
    {
      return $this->price;
    }
    public function setPrice($price)
    {
      $this->price = $price;
    }
    public function getPriceNegotiability()
    {
      return $this->priceNegotiability;
    }
    public function setPriceNegotiability($priceNegotiability)
    {
      $this->priceNegotiability = $priceNegotiability;
    }
    public function getSoldStatus()
    {
      return $this->soldStatus;
    }
    public function setSoldStatus($soldStatus)
    {
      $this->soldStatus = $soldStatus;
    }
    public function getSoldDate()
    {
      return $this->soldDate;
    }
    public function setSoldDate($soldDate)
    {
      $this->soldDate = $soldDate;
}
}


?>
