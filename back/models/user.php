<?php
  class User
  {
    private $userId;
    private $firstName;
    private $lastName;
    private $email;
    private $password;
    private $addressOne;
    private $contactNo;
    private $saleIds;

    public function __construct($firstName, $lastName, $email, $password, $addressOne, $contactNo){
      $this->firstName = $firstName;
      $this->lastName = $lastName;
      $this->email = $email;
      $this->password = $password;
      $this->addressOne = $addressOne;
      $this->contactNo = $contactNo;
    }

    public function getUserId()
    {
      return $this->userId;
    }
    public function setUserId($userId)
    {
      $this->userId = $userId;
    }
    public function getFirstName()
    {
      return $this->firstName;
    }
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
    }
    public function getLastName()
    {
      return $this->lastName;
    }
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
    }
    public function getEmail()
    {
      return $this->email;
    }
    public function setEmail($email)
    {
      $this->email = $email;
    }
    public function getPassword()
    {
      return $this->password;
    }
    public function setPassword($password)
    {
      $this->password = $password;
    }
    public function getAddressOne()
    {
      return $this->addressOne;
    }
    public function setAddressOne($addressOne)
    {
      $this->addressOne = $addressOne;
    }
    public function getContactNo()
    {
      return $this->contactNo;
    }
    public function setContactNo($contactNo)
    {
      $this->contactNo = $contactNo;
    }
    public function getSaleIds()
    {
      return $this->saleIds;
    }
    public function setSaleIds($saleIds)
    {
      $this->saleIds = $saleIds;
    }
}

?>
