<?php
  public class Purchase
  {
    private $purchaseKey;
    private $purchaseTimeStamp;
    private $buyerId;
    private $itemId;
    function __construct($purchaseKey,$purchaseTimeStamp,$buyerId,$itemId)
    {
      $this->purchaseKey = $purchaseKey;
      $this->purchaseTimeStamp = $purchaseTimeStamp;
      $this->buyerId = $buyerId;
      $this->itemId = $itemId;
    }

    //the getters and setters
    public function getPurchaseKey()
    {
      return $this->purchaseKey;
    }
    public function setPurchaseKey($purchaseKey)
    {
      $this->purchaseKey = $purchaseKey;
      return $this;
    }
    public function getPurchaseTimeStamp()
    {
      return $this->purchaseTimeStamp;
    }
    public function setPurchaseTimeStamp($purchaseTimeStamp)
    {
      $this->purchaseTimeStamp = $purchaseTimeStamp;
      return $this;
    }
    public function getBuyerId()
    {
      return $this->buyerId;
    }
    public function setBuyerId($buyerId)
    {
      $this->buyerId = $buyerId;
      return $this;
    }
    public function getItemId()
    {
      return $this->itemId;
    }
    public function setItemId($itemId)
    {
      $this->itemId = $itemId;
      return $this;
    }
}

?>
