<?php

	$products = "";
	if (MSG_TYPE == "Buyer Messages") {
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$uId = USER_ID_AUTH;
		$sql ="SELECT * FROM `on_sale_items` WHERE seller = '$uId' ORDER BY date_added Asc";
		
			$result = $dbConn->query($sql);
			//$res=$result->fetch_object();
			include './back/models/onSaleItem.php';
			$products=array();
			while($row = $result->fetch_assoc()) {
				$osi = new OnSaleItem();
				$osi->getDetailsTwo($row['item_id'], $row['price'], $row['item_images_json']);
				$products[] = $osi;
			}

			$result -> free_result();
      	    $dbConn->close();


	}
	elseif (MSG_TYPE == "Seller Messages") {
		require_once('./back/db/dbConnect.php');
		$dbConn = connectDb();
		$uId = USER_ID_AUTH;
		$sql0 = "SELECT order_id, ad_id FROM orders WHERE buyer_id = '$uId' ORDER BY date ASC";
		//echo $sql0;
		$result0 = $dbConn->query($sql0);
		//print_r($result0);

			include './back/models/onSaleItem.php';
			$productIds = array();
			$products=array();
			while ($row0 = $result0->fetch_assoc()) {
				$productId = $row0['ad_id'];
				//print_r($row0);
				array_push($productIds, $productId);
			}

			$result0 -> free_result();

       		 for ($i=0; $i < count($productIds); $i++) { 

       		 	$productId = $productIds[$i];
				$sql ="SELECT * FROM `on_sale_items` WHERE item_id = '$productId'";

				$result = $dbConn->query($sql);
				$row = $result->fetch_assoc();	
				$osi = new OnSaleItem();
				$osi->getDetailsTwo($row['item_id'], $row['price'], $row['item_images_json']);
				$osi->setSoldStatus($row['sold_status']);
				$products[] = $osi;
				$result -> free_result();
       		 }
       		 $dbConn->close();

	}

		for ($i=0; $i < count($products) ; $i++) { 
			$item = $products[$i];
			$salesId = $item->getItemId();
			$image = json_decode($item->getItemImagesJson());
			$soldStatus = $item->getSoldStatus();
				if ($soldStatus != "order_delivered") {
					
				
			
	?>
			<div class="userMsgAdBox image-border-radius">
				<div class="ad-image-holder">
					<?php
						echo "<img src=\"./back/imageUpload/$image\" class=\"modify-ad-image\">";
					?>
				</div>
				<h3 class="ad-price">NRs. <?php echo $item->getPrice(); ?></h3>
				<?php
				if (MSG_TYPE == "Buyer Messages") {
					echo "<button type=\"button\" onclick=\"showProductMsg('$salesId');\">View Messages</button>";
				}
				else{
					if ($soldStatus == "order_accepted") {
						echo "<span class=\"orderAcceptedNotice\">Seller has accepted your order. Please view message from Seller to learn about further deals.</span>";
						echo "<button type=\"button\" class=\"orderAcceptedBtn\"onclick=\"showSellerMsg('$salesId');\">View Seller Message</button>";
					}
					else{
						echo "<span>Seller has not accepted your order yet.</span>";
					}
				}

				?>
			</div>
	<?php
		}
	}
?>

