<?php
$ad_option = ADVERTISEMENT_OPTION;
?>

<div class="sorting-box col-md-12">
						<label>Sort By</label><br>
						<label>By Date:</label>
						<?php echo "<select id=\"dateSorting\" class=\"ad-display-sorting\" onchange=\"sortByDate('$ad_option');\">";?>
							<option value="newer">Newer First</option>
							<option value="older">Older First</option>

						</select><br>

						<label>By Confirmation:</label>
						<?php echo "<select id=\"priceSorting\" class=\"ad-display-sorting\" onchange=\"sortByPrice('$ad_option');\">";?>
							<option value="highest">All</option>
							<option value="highest">Confirmed</option>
							<option value="lowest">Suspected</option>
						</select>
					</div>
					<br><br>
					<h1>Reported animal issues at farms:</h1>
<?php
	require_once('./back/db/buyerDb.php');
	$bdb = new BuyerDb();
	$result = $bdb->getItemsBy(ADVERTISEMENT_OPTION, PRICE_DIRECTION, DATE_DIRECTION);

	while($row = $result->fetch_assoc()) {
		$issueId = $row['issue_report_id']
?>
		<div class="ad-box col-md-2 image-border-radius">
			<div class="ad-image-holder">
				<?php
					echo "<img src=\"./back/imageUpload/animals.jpeg\" class=\"modify-ad-image\">";
				?>
			</div>
			<br>
			<p><b>Animal: </b><?php echo $row['animal']; ?> <br>
				<b>Issue: </b><?php echo $row['report_subject']; ?> <br>
				<b>Reported on: </b><?php echo $row['reported_at']; ?> <br>
				<b>Incident of/since: </b><?php echo $row['incident_of_or_since']; ?> <br>
				<b>Confirmation Status: </b><?php echo $row['expert_issue_confirmation']; ?> <br>
			</p>
			<?php
			echo "<button type=\"button\" onclick=\"showProductDetails('$issueId');\">Details</button>";
			?>
		</div>
<?php
	}
?>
