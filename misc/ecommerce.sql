-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2021 at 09:01 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment_types`
--

CREATE TABLE `comment_types` (
  `type_id` varchar(15) NOT NULL,
  `type_description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_types`
--

INSERT INTO `comment_types` (`type_id`, `type_description`) VALUES
('comment', 'not reply'),
('reply', 'not direct comment');

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `category_id` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`category_id`, `description`) VALUES
('Car', NULL),
('Mobile', '0'),
('Motorcycle', '0'),
('Scooter', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_comments`
--

CREATE TABLE `item_comments` (
  `comment_id` varchar(30) NOT NULL,
  `datetime` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `comment_type_id` varchar(15) NOT NULL,
  `commenter` varchar(30) NOT NULL,
  `commenter_name` varchar(50) NOT NULL,
  `reply_to` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_comments`
--

INSERT INTO `item_comments` (`comment_id`, `datetime`, `comment`, `item_id`, `comment_type_id`, `commenter`, `commenter_name`, `reply_to`) VALUES
('1607591003', 2020, 'testttttt', 'pid-1606466010', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1607591451', 2020, 'exchange with opp v9??', 'pid-1606466010', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1608391909', 2020, 'Exchange with Iphone?', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608392014', 2020, 'ok', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608392481', 2020, 'exchange with Samsung?', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608401390', 2020, 'okokok', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392014'),
('1608401754', 2020, 'which model?', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608404061', 2020, 'noooooooo', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591451'),
('1608404072', 2020, 'which series?', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608391909'),
('1608406371', 2020, 'repliessss...', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608406389', 2020, 'no i dont want iphone', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608391909'),
('1608406461', 2020, 'reply 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608407147', 2020, 'noooo', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392014'),
('1608407189', 2020, 'not intrested in oppo. sorry.', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591451'),
('1608659249', 2020, 'how much warranty left?', 'pid-1606466010', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609007986', 2020, 'Is everything okay?', 'pid-1604686451', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609153581', 2020, 'Is it in good condition?', 'pid-1606460612', 'comment', 'uid-1603199592', 'Ritesh', NULL),
('1609153600', 2020, 'Wanna exchange with samsun s7?', 'pid-1606460612', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609414804', 2020, 'How much kilometer this bike ran?', 'pid-1609414758', 'comment', 'uid-1609180623', 'Ram', NULL),
('1609525830', 2021, 'Is it in good condition?', 'pid-1609524392', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1609525855', 2021, 'Yes, Evrything is fine.', 'pid-1609524392', 'reply', 'uid-1609180623', 'Ram', '1609525830'),
('1609525873', 2021, 'Yes Everything is Fine.', 'pid-1609524392', 'reply', 'uid-1609180623', 'Ram', '1609525830'),
('1609944928', 2021, 'reply 3', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1609944990', 2021, '5 months\n', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609945001', 2021, '5 month', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609945010', 2021, 'test 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945016', 2021, 'test 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945022', 2021, 'test3', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945036', 2021, '6 month', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609956355', 2021, 'Is it on good condition?', 'pid-1609956246', 'comment', 'uid-1603199592', 'Ritesh', NULL),
('1609956373', 2021, 'Yes perfectly fine', 'pid-1609956246', 'reply', 'uid-1602835691', 'gyurme', '1609956355'),
('1609956382', 2021, 'yes perfectly fine', 'pid-1609956246', 'reply', 'uid-1602835691', 'gyurme', '1609956355'),
('1609957052', 2021, 'Is everything fine?', 'pid-1609957035', 'comment', 'uid-1609180623', 'Ram', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `on_sale_items`
--

CREATE TABLE `on_sale_items` (
  `item_id` varchar(30) NOT NULL,
  `adv_title` varchar(50) NOT NULL,
  `item_images_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `price` int(11) NOT NULL,
  `sold_status` varchar(30) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `seller` varchar(30) NOT NULL,
  `category_id` varchar(30) NOT NULL,
  `date_sold` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `price_negotiability` varchar(3) NOT NULL,
  `brand_name` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `accepted_order_id` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `on_sale_items`
--

INSERT INTO `on_sale_items` (`item_id`, `adv_title`, `item_images_json`, `price`, `sold_status`, `date_added`, `seller`, `category_id`, `date_sold`, `price_negotiability`, `brand_name`, `description`, `accepted_order_id`) VALUES
('pid-1603994085', 'nifnosi', '\"advertiseImage/pid-1603994085.png\"', 5151, NULL, '2020-10-29 13:09:45', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Hunk', '', NULL),
('pid-1604165109', 'Bike on Sell', '\"advertiseImage/pid-1604165109.jpg\"', 51651, NULL, '2020-10-31 12:40:09', 'uid-1603128626', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Hunk', '', NULL),
('pid-1604165222', 'Scooter on Sell', '\"advertiseImage/pid-1604165222.png\"', 51666, 'order_placed', '2020-10-31 12:42:02', 'uid-1603128626', 'Scooter', '0000-00-00 00:00:00', 'No', 'Pleasure', '', NULL),
('pid-1604165255', 'Car on Sell', '\"advertiseImage/pid-1604165255.jpg\"', 145315, NULL, '2020-10-31 12:42:35', 'uid-1603128626', 'Car', '0000-00-00 00:00:00', 'No', 'Scorpio', '', NULL),
('pid-1604173500', 'Seeling my Mobile', '\"advertiseImage/pid-1604173500.jpg\"', 56416, NULL, '2020-10-31 15:00:00', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Pixel', '', '1609149805'),
('pid-1604173545', 'Seeling my Bike', '\"advertiseImage/pid-1604173545.jpg\"', 564848, 'order_placed', '2020-10-31 15:00:45', 'uid-1602835691', 'Car', '0000-00-00 00:00:00', 'No', 'Scorpio', '', NULL),
('pid-1604686451', 'Sccoter On sale', '\"advertiseImage/pid-1604686451.jpg\"', 154780, NULL, '2020-11-06 13:29:11', 'uid-1602835691', 'Scooter', '0000-00-00 00:00:00', 'Yes', 'Pleasure', '', '1609009400'),
('pid-1606460612', 'Description testing', '\"advertiseImage/pid-1606460612.jpg\"', 15151515, 'order_placed', '2020-11-27 02:18:32', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'OnePlus', 'Description testinhgggggg', NULL),
('pid-1606466010', 'One plus mobile on sale.', '\"advertiseImage/pid-1606466010.jpg\"', 35000, 'order_placed', '2020-11-27 03:48:30', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'No', 'OnePlus', 'I want to sell my one plus mobile as i have plan to buy new phone after selling this mobile. Mobile is ok no damage. Warrant card is also available(4 month remaining). Also with back cover.', '1608919925'),
('pid-1608485821', 'immmmm', '\"advertiseImage/pid-1608485821.jpg\"', 2, 'order_placed', '2020-12-20 12:52:01', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'OnePlus', 'ajkdakjndakasasasasasasasasasasa', '1609148579'),
('pid-1609182796', 'For order status testing', '\"advertiseImage/pid-1609182796.png\"', 123, 'order_placed', '2020-12-28 14:28:16', 'uid-1608659204', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Pixel', 'qqq', '1609182825'),
('pid-1609325271', 'for order test', '\"advertiseImage/pid-1609325271.jpg\"', 123333333, 'order_delivered', '2020-12-30 06:02:51', 'uid-1602835691', 'Car', '0000-00-00 00:00:00', 'Yes', 'Mazda', 'qqqqqqq', '1609325295'),
('pid-1609330913', 'qqqqq', '\"advertiseImage/pid-1609330913.png\"', 2147483647, 'order_delivered', '2020-12-30 07:36:53', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Splender', 'wdqada', '1609330939'),
('pid-1609414758', 'Splender on Sale', '\"advertiseImage/pid-1609414758.jpg\"', 45000, 'order_delivered', '2020-12-31 06:54:18', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Splender', 'Splender Bike is on sale. All taxes is paid. Bike is on perfect condition.', '1609414840'),
('pid-1609521772', 'Mobile on sale', '\"advertiseImage/pid-1609521772.png\"', 35000, 'order_delivered', '2021-01-01 12:37:52', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'No', 'Iphone', 'Mobile on sale. Like brand new', '1609521814'),
('pid-1609524392', 'Motorcycle on sale', '\"advertiseImage/pid-1609524392.jpg\"', 350000, NULL, '2021-01-01 13:21:32', 'uid-1609180623', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Beneli', 'Bike on sale for urgent purpose.', NULL),
('pid-1609956095', 'security testtttt', '\"advertiseImage/pid-1609956095.jpg\"', 122112, NULL, '2021-01-06 13:16:35', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'pulsar', 'aaa', NULL),
('pid-1609956246', 'Bike on sale', '\"advertiseImage/pid-1609956246.jpg\"', 55554, 'order_delivered', '2021-01-06 13:19:06', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'R15', 'mjjj', '1609956273'),
('pid-1609956825', 'Ritesh Ad', '\"advertiseImage/pid-1609956825.jpg\"', 45000, 'order_delivered', '2021-01-06 13:28:45', 'uid-1603199592', 'Car', '0000-00-00 00:00:00', 'No', 'Mazda', 'aaa', '1609956850'),
('pid-1609957035', 'rttt', '\"advertiseImage/pid-1609957035.jpg\"', 123324444, 'order_delivered', '2021-01-06 13:32:15', 'uid-1603199592', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Redimi', 'aa', '1609957061'),
('pid-1609957652', 'Mobile On sale', '\"advertiseImage/pid-1609957652.jpg\"', 144788, 'order_delivered', '2021-01-06 13:42:32', 'uid-1603199592', 'Mobile', '0000-00-00 00:00:00', 'No', 'LG', 'aaaa', '1609957687'),
('pid-1609957894', 'aaaa', '\"advertiseImage/pid-1609957894.jpg\"', 111, NULL, '2021-01-06 13:46:34', 'uid-1603199592', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'R15', 'aa', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_msg` varchar(500) NOT NULL,
  `order_acceptence_msg` varchar(500) DEFAULT NULL,
  `buyer_id` varchar(30) NOT NULL,
  `ad_id` varchar(30) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  `feedback` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `date`, `order_msg`, `order_acceptence_msg`, `buyer_id`, `ad_id`, `status`, `rating`, `feedback`) VALUES
('1608918681', '2020-12-25 13:06:21', 'i liked your order. i am ready to buy in 35000nrs. contact me.', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1608919918', '2020-12-25 13:26:58', 'testtttttttttttt', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1608919925', '2020-12-25 13:27:05', 'wwwwwwwww', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1609009400', '2020-12-26 14:18:20', 'i m ready to buy contact me.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609009432', '2020-12-26 14:18:52', 'i m ready to buy contact me. Let\'s talk.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609009521', '2020-12-26 14:20:21', 'i m ready to buy contact me. Let\'s talk. 2222', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609148579', '2020-12-28 04:57:59', 'I would like buy this mobile in this price.', NULL, 'uid-1608659204', 'pid-1608485821', 'order_placed', NULL, NULL),
('1609149745', '2020-12-28 05:17:25', 'Ok I am ready to buy this scooter.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609149805', '2020-12-28 10:03:48', 'Ok I am ready to buy...', 'ok contact mee. my number 4565165', 'uid-1608659204', 'pid-1604173500', 'order_placed', NULL, NULL),
('1609153627', '2020-12-28 11:25:47', 'Ok brother i want ti buy this phone please give me your contact details.', 'ok here is my contact number. 846515', 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153676', '2020-12-28 06:22:56', 'Ok I am ready to buy this phone by tommorow. Full cash deal.', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153859', '2020-12-28 06:25:59', 'Ok I am ready to buy this phone by tommorow. Full cash dealbdaiudnhaidjba', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153891', '2020-12-28 06:26:31', 'Ok I am ready to buy this phone by tommorow. Full cash dealbdaiudnhaidjba', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609155962', '2020-12-28 07:01:02', 'hhhhhhhhhhhhhhhhhh', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609155978', '2020-12-28 07:01:18', 'hhhhhhhhhhhhhhhhhh', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156144', '2020-12-28 07:04:04', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156266', '2020-12-28 07:06:06', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156376', '2020-12-28 07:07:56', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609175521', '2020-12-28 12:27:01', 'test1', NULL, 'uid-1608659204', 'pid-1604173545', 'order_placed', NULL, NULL),
('1609180651', '2020-12-28 13:52:31', 'I would like to buy.', NULL, 'uid-1609180623', 'pid-1604165222', 'order_placed', NULL, NULL),
('1609181786', '2020-12-28 14:11:26', 'so cheap i want to buy right now.', NULL, 'uid-1609180623', 'pid-1608485821', 'order_placed', NULL, NULL),
('1609182825', '2020-12-28 19:14:25', 'for testingggggggggg', 'ok lets do deal.', 'uid-1609180623', 'pid-1609182796', 'order_placed', NULL, NULL),
('1609324525', '2020-12-30 05:50:25', 'i would like to buy.', NULL, 'uid-1602835691', 'pid-1609182796', 'order_placed', NULL, NULL),
('1609324819', '2020-12-30 05:55:19', 'dkjabdlkandlk', NULL, 'uid-1609180623', 'pid-1606466010', 'order_placed', NULL, NULL),
('1609325295', '2021-01-01 15:30:17', 'Ok i would like to buy.', 'Ok then here is my contact number xxxxxx.', 'uid-1609180623', 'pid-1609325271', 'order_placed', 4, 'Car is good.'),
('1609330939', '2021-01-01 15:32:51', 'yesss', 'ok hreeee', 'uid-1609180623', 'pid-1609330913', 'order_placed', 5, 'okokokok'),
('1609414840', '2021-01-01 15:16:42', 'Dear Seller, I would like to buy your Bike on hand to cash. Contact me.', 'Ok here is my contact details: 985671254 contact me for buying this bike.', 'uid-1609180623', 'pid-1609414758', NULL, 3, 'sajan'),
('1609521814', '2021-01-01 17:24:41', 'oadjaoindoia', 'ok contact number is here:5468541', 'uid-1608659204', 'pid-1609521772', NULL, 5, 'It was a great deal. mobile was on condition.'),
('1609956273', '2021-01-06 18:07:58', 'orderrrr', 'okkkkk', 'uid-1609180623', 'pid-1609956246', NULL, 4, 'It is good'),
('1609956850', '2021-01-06 18:14:45', 'aaaaaa', 'aaaaaaaa', 'uid-1609180623', 'pid-1609956825', NULL, 0, 'aaa'),
('1609957061', '2021-01-06 18:24:40', 'i need', 'okk', 'uid-1609180623', 'pid-1609957035', NULL, 1, 'kkkk'),
('1609957687', '2021-01-06 18:29:11', 'aaaaaa', 'aaaa', 'uid-1609180623', 'pid-1609957652', NULL, 1, 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` varchar(30) NOT NULL,
  `date` int(11) NOT NULL,
  `buyer_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `saved_ads`
--

CREATE TABLE `saved_ads` (
  `id` int(15) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `ad_id` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_ads`
--

INSERT INTO `saved_ads` (`id`, `uid`, `ad_id`, `date`) VALUES
(11, 'uid-1602835691', 'pid-1606466010', '2021-01-06 17:54:48'),
(12, 'uid-1603199592', 'pid-1609956246', '2021-01-06 18:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `seller_feedback`
--

CREATE TABLE `seller_feedback` (
  `satisfaction_score` int(11) NOT NULL,
  `buyer_comment` int(11) NOT NULL,
  `seller_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `purchase_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sold_status`
--

CREATE TABLE `sold_status` (
  `sold_status_id` varchar(30) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sold_status`
--

INSERT INTO `sold_status` (`sold_status_id`, `description`) VALUES
('order_accepted', NULL),
('order_delivered', NULL),
('order_placed', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userreg`
--

CREATE TABLE `userreg` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `address_district` varchar(20) NOT NULL,
  `address_area` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userreg`
--

INSERT INTO `userreg` (`id`, `first_name`, `last_name`, `email`, `password`, `mobile_no`, `address_district`, `address_area`) VALUES
(117659, 'Sajan', 'Pyatha', 'admin', 'sajan', '9860590345', 'Bhaktapur', 'Thimi'),
(152067, 'Gyurme', 'Sherpa', 'gyurme@gmail.com', 'gyurme', '8749824799', 'Boudha', 'Chahbil'),
(304650, 'Sandis', 'Prajapati', 'sajandis.prajapati@gmail.com', 'sandis', '9898989898', 'Kathmandu', 'Bansbari');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(30) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `primary_email` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `contact_no` varchar(10) NOT NULL,
  `address` varchar(256) NOT NULL,
  `varification_doc_count` int(11) DEFAULT NULL,
  `varification_status` int(11) DEFAULT NULL,
  `sale_ids` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `f_name`, `l_name`, `primary_email`, `password`, `contact_no`, `address`, `varification_doc_count`, `varification_status`, `sale_ids`) VALUES
('uid-1602835691', 'gyurme', 'sherpa', 'gyurme.sp@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '014875264', 'boudha', NULL, NULL, ''),
('uid-1603128626', 'sajan', 'pyatha', 'sajanpathya@gmail.com', '7167c9b6c70495d4346cd24bed9691d684376552dd506c60152e1f4c482880cb', '1651465', 'hvjkbk', NULL, NULL, ''),
('uid-1603199592', 'Ritesh', 'Rai', 'ritesh@gmail.com', '84696d86eb6abe2025ed83e54fd192202917fe6a305c989fff0a616e300680b1', '456415', 'Chahbil', NULL, NULL, ''),
('uid-1608391869', 'Bishal', 'Shrestha', 'bishal@gmail.com', '9cb18b422c54cc80c27bd5c3f332a53360a9d5f876d5ca6a34a504ba25643926', '7849561161', 'Patan', NULL, NULL, ''),
('uid-1608659204', 'Asik', 'Karmacharya', 'asik@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '989898989', 'Hetauda', NULL, NULL, NULL),
('uid-1608918638', 'Bishu', 'Prajapati', 'bishu@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '999999999', 'gggg', NULL, NULL, NULL),
('uid-1609180623', 'Ram', 'Sharma', 'ram99@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '849645', 'kkkkkk', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_varification`
--

CREATE TABLE `user_varification` (
  `document_name` int(11) NOT NULL,
  `document_image_blob` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment_types`
--
ALTER TABLE `comment_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `comment_type_id` (`comment_type_id`),
  ADD KEY `commenter` (`commenter`),
  ADD KEY `reply_to` (`reply_to`);

--
-- Indexes for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `accepted_order_id` (`accepted_order_id`),
  ADD KEY `seller` (`seller`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `on_sale_items_ibfk_4` (`sold_status`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ad_id` (`ad_id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `purchase_id` (`purchase_id`);

--
-- Indexes for table `sold_status`
--
ALTER TABLE `sold_status`
  ADD PRIMARY KEY (`sold_status_id`);

--
-- Indexes for table `userreg`
--
ALTER TABLE `userreg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `primary_email` (`primary_email`);

--
-- Indexes for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `saved_ads`
--
ALTER TABLE `saved_ads`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `userreg`
--
ALTER TABLE `userreg`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=304651;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD CONSTRAINT `item_comments_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `item_comments_ibfk_2` FOREIGN KEY (`comment_type_id`) REFERENCES `comment_types` (`type_id`),
  ADD CONSTRAINT `item_comments_ibfk_3` FOREIGN KEY (`commenter`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `item_comments_ibfk_4` FOREIGN KEY (`reply_to`) REFERENCES `item_comments` (`comment_id`);

--
-- Constraints for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD CONSTRAINT `on_sale_items_ibfk_1` FOREIGN KEY (`seller`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `on_sale_items_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `item_category` (`category_id`),
  ADD CONSTRAINT `on_sale_items_ibfk_3` FOREIGN KEY (`accepted_order_id`) REFERENCES `orders` (`order_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `on_sale_items_ibfk_4` FOREIGN KEY (`sold_status`) REFERENCES `sold_status` (`sold_status_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`);

--
-- Constraints for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD CONSTRAINT `saved_ads_ibfk_1` FOREIGN KEY (`ad_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `saved_ads_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD CONSTRAINT `seller_feedback_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`purchase_id`);

--
-- Constraints for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD CONSTRAINT `user_varification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
